# Projeto Final - Grupo 10
Este repositório contem a implementação do projeto final para a disciplina SCC0541 - Laboratório de base de dados, construída pelo Grupo 10. Os autores são os que seguem:
## GRUPO 10
- Caroline Jesuíno Nunes da Silva - 9293925
- Guilherme Filipe Feitosa dos Santos - 11275092
- Julia Diniz Ferreira- 9364865
- Leonardo Giovanni Prati - 8300079

## Instruções para execução
### Requisitos
O projeto exige que você tenha instalado o [Docker](https://www.docker.com/) e o [Docker Compose](https://docs.docker.com/compose/install/) em seu computador.
### Execução
- Garanta que o Docker e o Docker Compose estejam instalados no seu computador.
- Garanta que os submodulos foram inicializados corretamente.
```
git clone https://gitlab.com/leonardo.prati/scc0541-projeto-final-deploy.git
cd scc0541-projeto-final-deploy
git submodule init
git submodule update
```
- Execute o comando `docker-compose up` para inicializar o projeto.
```
sudo docker-compose up
```
- Inicializa a base de dados através da interface gráfica do adminer.
### Inicializando a base de dados através do adminer
- Abra o adminer no endereço http://localhost:8080
- Selecione `PostgreSQL` na opção `System`, `db` no campo `Server` e realize o login com o usuário `postgres` e a senha `example`.
![image](doc/adminer_login.png)
- Crie uma base de dados chamada `pf` (case sensitive)
![image](doc/adminer_create_database_1.png)
![image](doc/adminer_create_database_2.png)
- Dentro da base de dados recém criada, importe e execute o arquivo [scc0541-projeto-final-base-de-dados/scripts/generate_database.sql](scc0541-projeto-final-base-de-dados/scripts/generate_database.sql)
![image](doc/adminer_import_1.png)
![image](doc/adminer_import_2.png)
- Você deverá receber uma mensagem de sucesso.
![image](doc/adminer_final.png)

## Frontend
- Abra o navegador e acesse o endereço ????


## Backend
### Descrição das pontas do projeto

----
## Autenticação
  - Método: `POST`
  - Endpoint: `/login`
  - Body:
```
{
    username: string
    password: string
}
```
  - Retorno:
```
{
    username: string
    role: string
    original_id: int
}
```
  - 401 se o usuário não existir ou a senha estiver incorreta.
  - 500 se ocorrer algum erro ao autenticar.
----
## Driver

Tipo de dado:
#### Driver
```
{
    driverId: int
    driverRef: string
    number: int
    code: string
    forename: string
    surname: string
    dob: string
    nationality: string
    url: string
}
```
----
  - Método: `GET`
  - Endpoint: `/driver`
  - Retorno: array[[Driver](#driver)]
  - 500 se ocorreu algum erro
----
  - Método: `GET`
  - Endpoint: `/driver?count_only=true`
  - Retorno: 
   ```
    {
        drivers_count: int
    }
   ```
  }
  - 500 se ocorreu algum erro
----
  - Método: `GET`
  - Endpoint: `/driver/:driver_id`
  - Retorno: [Driver](#driver)
  - 404 se o piloto não existir
  - 500 se ocorreu algum erro
----
  - Método: `GET`
  - Endpoint: `/driver/:driver_id/wins`
  - Retorno: 
  ```
  {
    wins: int
  }
  ```
  - 404 se o piloto não existir
  - 500 se ocorreu algum erro
----
  - Método: `GET`
  - Endpoint: `/driver/:driver_id/years`
  - Retorno: 
  ```
  {
    first_year: int,
    last_year: int
  }
  ```
  - 404 se o piloto não existir
  - 500 se ocorreu algum erro
----
  - Método: `GET`
  - Endpoint: `/driver/:driver_id/wins_by_year`
  - Retorno: 
  ```
  array[{
    Ano: string
    Nome da Corrida: string
    Vitórias: int
  }]
  ```
  - 404 se o piloto não existir
  - 500 se ocorreu algum erro
----
  - Método: `GET`
  - Endpoint: `/driver/wins_by_constructor/:constructor_id`
  - Retorno:
  ```
  {
    driverId: string
    forename: string
    surname: string
    poles: int
  }
  ```
  - 404 se o constructor não existir
  - 500 se ocorreu algum erro
----
  - Método: `POST`
  - Endpoint: `/driver`
  - Body:
```
{
    driverRef: string
    number: int
    code: string
    forename: string
    surname: string
    dob: string
    nationality: string
    url: string
}
```
----
## Constructor
----
  - Método: `GET`
  - Endpoint: `/constructor`
  - Retorno: 
  ```
  array[{
    constructorId: int
    constructorRef: str
    name: str
    nationality: str
    url: str
  }]
  ```
  - 500 se ocorreu algum erro
----
  - Método: `GET`
  - Endpoint: `/constructor/:constructor_id`
  - Retorno: 
  ```
  {
    constructorId: int
    constructorRef: str
    name: str
    nationality: str
    url: str
  }
  ```
  - 404 se o construtor não foi encontrado
  - 500 se ocorreu algum erro
----
  - Método: `GET`
  - Endpoint: `/constructor/:constructor_id/wins`
  - Retorno: 
  ```
  {
    wins: int
  }
  ```
  - 500 se ocorreu algum erro
----
  - Método: `GET`
  - Endpoint: `/constructor/:constructor_id/drivers`
  - Retorno: 
  ```
  {
    drivers: int
  }
  ```
  - 500 se ocorreu algum erro
----
  - Método: `GET`
  - Endpoint: `/constructor/:constructor_id/drivers/:driver_forename`
  - Retorno: 
  ```
  
  ```
  - 500 se ocorreu algum erro
----
  - Método: `GET`
  - Endpoint: `/constructor/:constructor_id/years`
  - Retorno: 
  ```
  {
    first_year: int,
    last_year: int
  }

  ```
  - 500 se ocorreu algum erro
----
  - Método: `POST`
  - Endpoint: `/constructor`
  - Body:
  ```
  {
    constructorRef: str
    name: str
    nationality: str
    url: str
  }

  ```
----
## Airport
----
  - Método: `GET`
  - Endpoint: `/airport/:city_name/distance`
  - Retorno: 
  ```
  array[{
    Nome do Aeroporto: string
    Nome da cidade: string
    Distância [km]: float
  }]
  ```
  - 500 se ocorreu algum erro
----

## Season
----
  - Método: `GET`
  - Endpoint: `/season/count`
  - Retorno: 
  ```
    {
        seasons_count: int
    }
  ```
  - 500 se ocorreu algum erro
----
## Race
----
  - Método: `GET`
  - Endpoint: `/race/count`
  - Retorno: 
  ```
    {
        races_count: int
    }
  ```
  - 500 se ocorreu algum erro
----
## Status
----
  - Método: `GET`
  - Endpoint: `/status`
  - Retorno: 
  ```
    array[
        {
            status: string,
            count: int
        }
    ]
  ```
  - 500 se ocorreu algum erro
----
- Método: `GET`
- Endpoint: `/status?driver_id=:driver_id`
- Retorno: 
```
array[
    {
        status: string,
        count: int
    }
]
```
- 500 se ocorreu algum erro
----
- Método: `GET`
- Endpoint: `/status?constructor_id=:constructor_id`
- Retorno: 
```
array[
    {
        status: string,
        count: int
    }
]
```
- 500 se ocorreu algum erro
----